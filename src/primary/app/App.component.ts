import { Component, Vue } from 'vue-property-decorator';

import { InteractiveMapVue } from '@/primary/interactive-map';
import { NavbarVue } from '@/primary/navbar';

@Component({
  components: { NavbarVue, InteractiveMapVue },
})
export default class AppComponent extends Vue {}
