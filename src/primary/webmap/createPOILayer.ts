import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';

import { createPOIStyle } from '@/primary/webmap/createPOIStyle';

export const createPOILayer = (): VectorLayer => {
  const vectorLayerOptions = {
    opacity: 1,
    visible: true,
    renderMode: 'image',
    updateWhileAnimating: false,
    updateWhileInteracting: false,
    source: new VectorSource({ features: [] }),
    style: createPOIStyle(),
  };

  return new VectorLayer(vectorLayerOptions);
};
