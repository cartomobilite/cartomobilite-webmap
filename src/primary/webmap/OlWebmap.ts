import { Control } from 'ol/control';
import GeoJSON from 'ol/format/GeoJSON';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import Map from 'ol/Map';
import { transformExtent } from 'ol/proj';

import { POIGeojsonFeatureCollection } from '@/primary/interactive-map/POIGeojsonFeatureCollection';
import { Webmap } from '@/primary/webmap/Webmap';

export class OlWebmap implements Webmap {
  constructor(private map: Map, private tileLayer: TileLayer, private poiLayer: VectorLayer) {
    map.addLayer(tileLayer);
    map.addLayer(poiLayer);
  }

  fit(extent: [number, number, number, number]): void {
    this.map.getView().fit(transformExtent(extent, 'EPSG:4326', 'EPSG:3857'));
  }

  setup(target: string): void {
    this.map.setTarget(target);
  }

  addControl(htmlElement: HTMLElement) {
    this.map.addControl(new Control({ element: htmlElement }));
  }

  getExtent(): [number, number, number, number] {
    const extent = this.map.getView().calculateExtent();
    return transformExtent(extent, 'EPSG:3857', 'EPSG:4326');
  }

  addPOIs(poiFeatureCollection: POIGeojsonFeatureCollection): void {
    const parser = new GeoJSON({ dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857' });
    this.poiLayer.getSource().clear();
    this.poiLayer.getSource().addFeatures(parser.readFeatures(poiFeatureCollection));
  }
}
