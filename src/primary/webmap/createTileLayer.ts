import TileLayer from 'ol/layer/Tile';
import XYZ from 'ol/source/XYZ';

export const createTileLayer = () => {
  const XYZOptions = {
    attributions: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    crossOrigin: 'anonymous',
    maxZoom: 18,
    minZoom: 0,
    url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png',
  };

  const tileLayerOptions = {
    opacity: 1,
    visible: true,
    source: new XYZ(XYZOptions),
  };

  return new TileLayer(tileLayerOptions);
};
