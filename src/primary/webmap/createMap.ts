import { View, Map } from 'ol';
import { defaults as defaultControls, Attribution, ScaleLine } from 'ol/control';
import { defaults as defaultInteractions } from 'ol/interaction';
import { transformExtent } from 'ol/proj';

export const createMap = (): Map => {
  const interactionOptions = {
    altShiftDragRotate: false,
    pinchRotate: false,
  };

  const controlOptions = {
    attribution: false,
    rotate: false,
  };

  const controlExtensions = [new Attribution({ collapsible: false }), new ScaleLine()];

  const viewOptions = {
    center: [0, 0],
    enableRotation: false,
    maxExtent: transformExtent([-180, 90, 180, -90], 'EPSG:4326', 'EPSG:3857'),
    maxZoom: 19,
    minZoom: 0,
    zoom: 3,
  };

  const mapOptions = {
    controls: defaultControls(controlOptions).extend(controlExtensions),
    interactions: defaultInteractions(interactionOptions),
    loadTilesWhileAnimating: false,
    loadTilesWhileInteracting: false,
    moveTolerance: 2,
    view: new View(viewOptions),
  };

  return new Map(mapOptions);
};
