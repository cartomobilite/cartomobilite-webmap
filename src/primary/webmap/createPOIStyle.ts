import { FeatureLike } from 'ol/Feature';
import { Fill, RegularShape, Style } from 'ol/style';

export const drawImage = (context: CanvasRenderingContext2D, image: HTMLImageElement) => () => {
  context.drawImage(image, 8, 8);
};

const createCache = (icons: string[], borderColors: string[]): { [name: string]: Style } => {
  const cache: { [name: string]: Style } = {};

  icons.forEach(icon => {
    borderColors.forEach(borderColor => {
      const regShape = new RegularShape({
        fill: new Fill({ color: [255, 255, 255, 1] }),
        radius: 16,
        points: 0,
      });

      const canvas = regShape.getImage(1) as HTMLCanvasElement;
      const ctx = canvas.getContext('2d');
      const image = new Image(18, 18);

      image.onload = drawImage(ctx!, image);
      image.src = `./icons/${icon}.svg`;

      ctx!.fillStyle = 'rgb(255, 255, 255)';
      ctx!.strokeStyle = borderColor;
      ctx!.lineWidth = 4;
      ctx!.beginPath();
      ctx!.arc(16, 16, 14, 0, 2 * Math.PI);
      ctx!.closePath();
      ctx!.stroke();
      ctx!.fill();

      cache[`${icon}${borderColor}`] = new Style({ image: regShape });
    });
  });

  return cache;
};

export const createPOIStyle = () => {
  const icons = ['bar', 'bus_stop', 'hotel', 'restaurant', 'supermarket', 'toilets'];
  const borderColors = ['#23d160', '#ffdd57', '#ff3860', '#cccccc'];
  const cache = createCache(icons, borderColors);

  return (olFeature: FeatureLike, res?: number) =>
    cache[`${olFeature.getProperties().style.icon}${olFeature.getProperties().style.borderColor}`];
};
