import { POIGeojsonFeatureCollection } from '@/primary/interactive-map/POIGeojsonFeatureCollection';

export interface Webmap {
  setup(target: string): void;
  fit(extent: [number, number, number, number]): void;
  addControl(htmlElement: HTMLElement): void;
  getExtent(): [number, number, number, number];
  addPOIs(poiFeatureCollection: POIGeojsonFeatureCollection): void;
}
