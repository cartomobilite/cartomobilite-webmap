import InteractiveMapComponent from './InteractiveMap.component';
import InteractiveMapVue from './InteractiveMap.vue';

export { InteractiveMapComponent, InteractiveMapVue };
