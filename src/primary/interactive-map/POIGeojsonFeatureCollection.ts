import { POIGeojsonFeature } from '@/primary/interactive-map/POIGeojsonFeature';

export interface POIGeojsonFeatureCollection {
  type: string;
  features: POIGeojsonFeature[];
}

export const toPOIGeojsonFeatureCollection = (poiGeojsonFeatures: POIGeojsonFeature[]) => ({
  type: 'FeatureCollection',
  features: poiGeojsonFeatures,
});
