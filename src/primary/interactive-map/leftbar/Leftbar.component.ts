import { Component, Inject, Vue } from 'vue-property-decorator';

@Component
export default class LeftbarComponent extends Vue {
  isOpen = false;

  open() {
    this.isOpen = true;
  }

  close() {
    this.isOpen = false;
  }
}
