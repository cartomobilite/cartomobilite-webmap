import LeftbarComponent from './Leftbar.component';
import LeftbarVue from './Leftbar.vue';

export { LeftbarComponent, LeftbarVue };
