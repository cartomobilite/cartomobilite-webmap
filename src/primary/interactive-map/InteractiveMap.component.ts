import { Component, Inject, Vue } from 'vue-property-decorator';

import { Logger } from '@/domain/Logger';
import { POI } from '@/domain/POI';
import { POIRepository } from '@/domain/POIRepository';
import { Profile } from '@/domain/Profile';
import { Service } from '@/domain/Service';
import { ComponentState } from '@/primary/ComponentState';
import { LeftbarVue } from '@/primary/interactive-map/leftbar';
import { toPOIGeojsonFeature } from '@/primary/interactive-map/POIGeojsonFeature';
import { POIGeojsonFeatureCollection, toPOIGeojsonFeatureCollection } from '@/primary/interactive-map/POIGeojsonFeatureCollection';
import { Webmap } from '@/primary/webmap/Webmap';

@Component({
  components: { LeftbarVue },
})
export default class Map extends Vue {
  state = ComponentState.PENDING;
  poifeatureCollection!: POIGeojsonFeatureCollection;

  @Inject()
  private webmap!: () => Webmap;

  @Inject()
  private logger!: () => Logger;

  @Inject()
  private poiRepository!: () => POIRepository;

  mounted() {
    const leftbarElement = this.$refs.leftbar as any;
    this.webmap().setup('interactive-map');
    this.webmap().addControl(leftbarElement.$el);
    this.webmap().fit([-149.63069915771484, -17.57080239428328, -149.5444393157959, -17.51908083559126]);

    this.poiRepository()
      .find(Service.BAR, Profile.WHEELCHAIR, this.webmap().getExtent())
      .then(pois => this.fromDomain(pois))
      .catch(error => this.error(error));
  }

  private fromDomain(pois: POI[]): void {
    this.poifeatureCollection = toPOIGeojsonFeatureCollection(pois.map(toPOIGeojsonFeature));
    this.webmap().addPOIs(this.poifeatureCollection);
    this.state = ComponentState.SUCCESS;
  }

  private error(error: Error): void {
    this.logger().error('Impossible to find POIs', error);
    this.state = ComponentState.ERROR;
  }
}
