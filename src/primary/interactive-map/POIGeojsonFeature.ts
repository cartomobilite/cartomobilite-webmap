import { AccessibilityLevel } from '@/domain/AccessibilityLevel';
import { POI } from '@/domain/POI';
import { Service } from '@/domain/Service';

export interface POIGeojsonFeature {
  id: string;
  type: 'Feature';
  properties: {
    service: string;
    accessibilityLevel: string;
    tags: { [name: string]: string };
    style: { icon: string; borderColor: string };
  };
  geometry: {
    type: 'Point';
    coordinates: [number, number];
  };
}

const toIcon = (service: Service) => {
  switch (service) {
    case Service.BAR:
      return 'bar';
    case Service.TOILETS:
      return 'toilets';
    case Service.SUPERMARKET:
      return 'supermarket';
    case Service.RESTAURANT:
      return 'restaurant';
    case Service.HOTEL:
      return 'hotel';
    case Service.BUS_STOP:
      return 'bus_stop';
  }
};

const toBorderColor = (accessibilityLevel: AccessibilityLevel) => {
  switch (accessibilityLevel) {
    case AccessibilityLevel.FULLY_ACCESSIBLE:
      return '#23d160';
    case AccessibilityLevel.PARTIALLY_ACCESSIBLE:
      return '#ffdd57';
    case AccessibilityLevel.NOT_ACCESSIBLE:
      return '#ff3860';
    case AccessibilityLevel.UNKNOWN:
      return '#cccccc';
  }
};

export const toPOIGeojsonFeature = (poi: POI): POIGeojsonFeature => ({
  id: poi.id,
  type: 'Feature',
  properties: {
    service: poi.service,
    accessibilityLevel: poi.accessibilityLevel,
    tags: poi.tags,
    style: { icon: toIcon(poi.service), borderColor: toBorderColor(poi.accessibilityLevel) },
  },
  geometry: { type: 'Point', coordinates: [poi.coordinates[0], poi.coordinates[1]] },
});
