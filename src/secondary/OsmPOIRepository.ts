import { AxiosInstance } from 'axios';
import Feature from 'ol/Feature';
import OSMXML from 'ol/format/OSMXML';
import { Point } from 'ol/geom';

import { AccessibilityLevel } from '@/domain/AccessibilityLevel';
import { NotFound } from '@/domain/NotFound';
import { POI } from '@/domain/POI';
import { POIRepository } from '@/domain/POIRepository';
import { Profile } from '@/domain/Profile';
import { Service } from '@/domain/Service';

export const toCondition = (service: Service) => {
  switch (service) {
    case Service.BAR:
      return 'amenity=bar';
    case Service.BUS_STOP:
      return 'highway=bus_stop';
    case Service.HOTEL:
      return 'tourism=hotel';
    case Service.RESTAURANT:
      return '"amenity"~"restaurant|fast_food"';
    case Service.SUPERMARKET:
      return 'shop=supermarket';
    case Service.TOILETS:
      return 'amenity=toilets';
  }
};

const toOlFeatures = (osmXML: string): Feature<Point>[] => {
  const osmXMLParser = new OSMXML();
  const readOptions = { dataProjection: 'EPSG:4326', featureProjection: 'EPSG:4326' };
  return osmXMLParser.readFeatures(osmXML, readOptions) as Feature<Point>[];
};

export const toAccessibilityLevel = (tagValue: string | undefined): AccessibilityLevel => {
  switch (tagValue) {
    case undefined:
      return AccessibilityLevel.UNKNOWN;
    case 'limited':
      return AccessibilityLevel.PARTIALLY_ACCESSIBLE;
    case 'yes':
      return AccessibilityLevel.FULLY_ACCESSIBLE;
    case 'no':
      return AccessibilityLevel.NOT_ACCESSIBLE;
    default:
      return AccessibilityLevel.UNKNOWN;
  }
};

export const toProfileAccessibilityLevel = (olFeature: Feature<Point>, profile: Profile): AccessibilityLevel => {
  switch (profile) {
    case Profile.BLIND:
      return toAccessibilityLevel(olFeature.get('blind'));
    case Profile.DEAF:
      return toAccessibilityLevel(olFeature.get('blind'));
    case Profile.ELDER:
      return toAccessibilityLevel(olFeature.get('elder'));
    case Profile.WHEELCHAIR:
      return toAccessibilityLevel(olFeature.get('wheelchair'));
  }
};

export const toPOI = (olFeature: Feature<Point>, profile: Profile, service: Service): POI => {
  let { geometry, ...properties } = olFeature.getProperties();
  return {
    id: olFeature.getId().toString(),
    service,
    accessibilityLevel: toProfileAccessibilityLevel(olFeature, profile),
    tags: properties,
    coordinates: olFeature.getGeometry().getCoordinates(),
  };
};

export class OsmPOIRepository implements POIRepository {
  constructor(private axiosInstance: AxiosInstance) {}

  find(service: Service, profile: Profile, extent: [number, number, number, number]): Promise<POI[]> {
    const requestExtent = [extent[1], extent[0], extent[3], extent[2]].join(',');
    const condition = toCondition(service);
    const overpassRequest = `?data=node[${condition}](${requestExtent});out+meta;`;
    return this.axiosInstance
      .get<string>(overpassRequest)
      .then(response => toOlFeatures(response.data))
      .then(olFeatures => olFeatures.map(olFeature => toPOI(olFeature, profile, service)))
      .catch(error => {
        throw new NotFound('POIs').cause(error);
      });
  }
}
