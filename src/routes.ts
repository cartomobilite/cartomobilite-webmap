import Vue from 'vue';
import VueRouter, { RouteConfig, RouterOptions } from 'vue-router';

Vue.use(VueRouter);

const routes: RouteConfig[] = [];

export const routerOptions: RouterOptions = {
  routes,
};
