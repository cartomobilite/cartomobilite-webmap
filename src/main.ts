import '@/styles/main.scss';
import axios from 'axios';
import Buefy from 'buefy';
import Vue from 'vue';
import VueRouter from 'vue-router';

import AppComponent from './primary/app/App.vue';
import { routerOptions } from './routes';

import { ConsoleLogger } from '@/primary/ConsoleLogger';
import { createMap } from '@/primary/webmap/createMap';
import { createPOILayer } from '@/primary/webmap/createPOILayer';
import { createTileLayer } from '@/primary/webmap/createTileLayer';
import { OlWebmap } from '@/primary/webmap/OlWebmap';
import { OsmPOIRepository } from '@/secondary/OsmPOIRepository';

Vue.use(Buefy);

Vue.config.productionTip = false;

const router = new VueRouter(routerOptions);
const logger = new ConsoleLogger(console); // eslint-disable-line no-console
const olWebmap = new OlWebmap(createMap(), createTileLayer(), createPOILayer());
const overpassAxios = axios.create({ baseURL: 'https://overpass-api.de/api/interpreter' });

new Vue({
  router,
  render: h => h(AppComponent),
  provide: {
    webmap: () => olWebmap,
    poiRepository: () => new OsmPOIRepository(overpassAxios),
    logger: () => logger,
  },
}).$mount('#app');
