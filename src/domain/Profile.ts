export enum Profile {
  BLIND = 'BLIND',
  DEAF = 'DEAF',
  ELDER = 'ELDER',
  WHEELCHAIR = 'WHEELCHAIR',
}
