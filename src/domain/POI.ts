import { AccessibilityLevel } from '@/domain/AccessibilityLevel';
import { Service } from '@/domain/Service';

export interface POI {
  id: string;
  service: Service;
  accessibilityLevel: AccessibilityLevel;
  tags: { [name: string]: string };
  coordinates: number[];
}
