import { POI } from '@/domain/POI';
import { Profile } from '@/domain/Profile';
import { Service } from '@/domain/Service';

export interface POIRepository {
  find(service: Service, profile: Profile, extent: [number, number, number, number]): Promise<POI[]>;
}
