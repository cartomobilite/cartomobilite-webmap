import { CustomError } from 'ts-custom-error';

export class CartomobiliteError extends CustomError {
  cause(error: Error): CartomobiliteError {
    if (this.stack && error.stack) {
      this.stack += `\nCaused by: ${error.stack}`;
    }
    return this;
  }
}
