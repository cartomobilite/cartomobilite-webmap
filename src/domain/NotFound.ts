import { CartomobiliteError } from '@/domain/CartomobiliteError';

export class NotFound extends CartomobiliteError {
  constructor(item: string) {
    super(`The item "${item}" was not found`);
  }
}
