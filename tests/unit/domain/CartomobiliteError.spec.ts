import { CartomobiliteError } from '@/domain/CartomobiliteError';

describe('AraxxeError', () => {
  it('Should be an instance of Error', () => {
    const cartomobiliteError = new CartomobiliteError();
    expect(cartomobiliteError).toBeInstanceOf(Error);
  });

  it('Should have a message and stack catch cause', () => {
    const cartomobiliteError = new CartomobiliteError('Error message').cause(new Error('Another error'));
    expect(cartomobiliteError.message).toBe('Error message');
    expect(cartomobiliteError.stack).toContain('Error message');
    expect(cartomobiliteError.stack).toContain('\nCaused by: Error');
    expect(cartomobiliteError.stack).toContain('Another error');
  });

  it('Should not add message without stack from deep error', () => {
    const withoutStack = new Error('Without stack');
    withoutStack.stack = undefined;
    const cartomobiliteError = new CartomobiliteError('Error message').cause(withoutStack);
    expect(cartomobiliteError.stack).not.toContain('undefined');
    expect(cartomobiliteError.stack).not.toContain('Caused by');
  });

  it('Should not add to stack without existing stack', () => {
    const cartomobiliteError = new CartomobiliteError('Error message');
    cartomobiliteError.stack = undefined;
    cartomobiliteError.cause(new Error('Another error'));
    expect(cartomobiliteError.stack).toBeUndefined();
  });
});
