import { AccessibilityLevel } from '@/domain/AccessibilityLevel';
import { Service } from '@/domain/Service';

export const makePOI = (overrides = {}) => ({
  id: 'test',
  service: Service.BAR,
  accessibilityLevel: AccessibilityLevel.FULLY_ACCESSIBLE,
  tags: { 'addr:housename': 'Fare Tony', amenity: 'bar', name: 'Bar du Fare Tony', note: 'Bar à bière' },
  coordinates: [-149.5687696, -17.5403938],
  ...overrides,
});
