import { createLocalVue, shallowMount } from '@vue/test-utils';
import Buefy from 'buefy';
import VueRouter from 'vue-router';

import { NavbarComponent, NavbarVue } from '@/primary/navbar';

describe('Navbar', () => {
  it('should be a Vue instance', () => {
    const localVue = createLocalVue();
    localVue.use(VueRouter);
    localVue.use(Buefy);
    const wrapper = shallowMount<NavbarComponent>(NavbarVue, {
      localVue,
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});
