import { createLocalVue, shallowMount, Wrapper } from '@vue/test-utils';
import Buefy from 'buefy';
import VueRouter from 'vue-router';

import { LeftbarComponent, LeftbarVue } from '@/primary/interactive-map/leftbar';

let wrapper: Wrapper<LeftbarComponent>;
let component: LeftbarComponent;

const wrap = () => {
  const localVue = createLocalVue();
  localVue.use(VueRouter);
  localVue.use(Buefy);
  wrapper = shallowMount<LeftbarComponent>(LeftbarVue, { localVue });
  component = wrapper.vm;
};

describe('Leftbar', () => {
  it('should be a Vue instance', () => {
    wrap();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should open leftbar on mobile', () => {
    wrap();
    expect(component.isOpen).toBeFalsy();
    component.open();
    expect(component.isOpen).toBeTruthy();
  });

  it('should close leftbar on mobile', () => {
    wrap();
    component.open();
    expect(component.isOpen).toBeTruthy();
    component.close();
    expect(component.isOpen).toBeFalsy();
  });
});
