import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { AppComponent, AppVue } from '@/primary/app';

describe('App', () => {
  it('should be a Vue instance', () => {
    const localVue = createLocalVue();
    localVue.use(VueRouter);
    const wrapper = shallowMount<AppComponent>(AppVue, {
      localVue,
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});
