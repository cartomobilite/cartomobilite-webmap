import { Map } from 'ol';
import { getCenter } from 'ol/extent';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import { transformExtent } from 'ol/proj';

import { createMap } from '@/primary/webmap/createMap';
import { createPOILayer } from '@/primary/webmap/createPOILayer';
import { createTileLayer } from '@/primary/webmap/createTileLayer';
import { OlWebmap } from '@/primary/webmap/OlWebmap';
import {
  POIGeojsonFeatureCollection,
  toPOIGeojsonFeatureCollection
} from '@/primary/interactive-map/POIGeojsonFeatureCollection';
import GeoJSON from 'ol/format/GeoJSON';
import { makePOI } from './POI.fixture';
import { toPOIGeojsonFeature } from '@/primary/interactive-map/POIGeojsonFeature';

let map: Map;
let tileLayer: TileLayer;
let poiLayer: VectorLayer;
let olWebmap: OlWebmap;

describe('OlWebmap', () => {
  beforeEach(() => {
    map = createMap();
    tileLayer = createTileLayer();
    poiLayer = createPOILayer();
    olWebmap = new OlWebmap(map, tileLayer, poiLayer);
  });

  it('should setup webmap', () => {
    olWebmap.setup('test');
    expect(map.getTarget()).toBe('test');
  });

  it('should add control', () => {
    const nbControls = map.getControls().getLength();
    const div = document.createElement('div');
    olWebmap.addControl(div);
    expect(map.getControls().getLength()).toBe(nbControls + 1);
  });

  it('should fit to given extent', () => {
    olWebmap.fit([15, 15, 15, 15]);
    expect(map.getView().getCenter()).toEqual(getCenter(transformExtent([15, 15, 15, 15], 'EPSG:4326', 'EPSG:3857')));
  });

  it('should return the current extent', () => {
    const extent: [number, number, number, number] = [15, 15, 15, 15];
    olWebmap.fit(extent);
    expect(olWebmap.getExtent().map(Math.round)).toEqual(extent);
  });

  it('should add POI features to POI layer', () => {
    const poi = makePOI();
    expect(poiLayer.getSource().getFeatures()).toHaveLength(0);
    olWebmap.addPOIs(toPOIGeojsonFeatureCollection([toPOIGeojsonFeature(poi)]));
    expect(poiLayer.getSource().getFeatures()).toHaveLength(1);
  });
});
