import { makePOI } from './POI.fixture';

import { AccessibilityLevel } from '@/domain/AccessibilityLevel';
import { Service } from '@/domain/Service';
import { toPOIGeojsonFeature } from '@/primary/interactive-map/POIGeojsonFeature';

const expectBorderColor = (accessibilityLevel: AccessibilityLevel, borderColor: string) =>
  expect(toPOIGeojsonFeature(makePOI({ accessibilityLevel })).properties.style.borderColor).toBe(borderColor);

const expectIcon = (service: Service, icon: string) => expect(toPOIGeojsonFeature(makePOI({ service })).properties.style.icon).toBe(icon);

describe('POIGeojsonFeature', () => {
  it('should set border color', () => {
    expectBorderColor(AccessibilityLevel.FULLY_ACCESSIBLE, '#23d160');
    expectBorderColor(AccessibilityLevel.PARTIALLY_ACCESSIBLE, '#ffdd57');
    expectBorderColor(AccessibilityLevel.NOT_ACCESSIBLE, '#ff3860');
    expectBorderColor(AccessibilityLevel.UNKNOWN, '#cccccc');
  });

  it('should set icon', () => {
    expectIcon(Service.BAR, 'bar');
    expectIcon(Service.RESTAURANT, 'restaurant');
    expectIcon(Service.TOILETS, 'toilets');
    expectIcon(Service.SUPERMARKET, 'supermarket');
    expectIcon(Service.HOTEL, 'hotel');
    expectIcon(Service.BUS_STOP, 'bus_stop');
  });
});
