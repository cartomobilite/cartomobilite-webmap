import { createMap } from '@/primary/webmap/createMap';

describe('createMap', () => {
  it('should create a map', () => {
    const map = createMap();
    expect(map.getView().getCenter()).toEqual([0, 0]);
    expect(map.getView().get('enableRotation')).toBeFalsy();
    expect(map.getView().getMaxZoom()).toBe(19);
    expect(map.getView().getMinZoom()).toBe(0);
    expect(map.getView().getZoom()).toBe(3);
  });
});
