import { XYZ } from 'ol/source';

import { createTileLayer } from '@/primary/webmap/createTileLayer';

describe('createTileLayer', () => {
  it('should create a tile layer', () => {
    const urls = [
      'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png',
      'https://b.tile.openstreetmap.org/{z}/{x}/{y}.png',
      'https://c.tile.openstreetmap.org/{z}/{x}/{y}.png',
    ];
    const tileLayer = createTileLayer();
    const source = tileLayer.getSource() as XYZ;
    expect(source.getUrls()).toEqual(urls);
    expect(tileLayer.getVisible()).toBeTruthy();
  });
});
