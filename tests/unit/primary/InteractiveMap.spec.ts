import sinon from 'sinon';

import { createLocalVue, shallowMount, Wrapper } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { dummyPromise } from '../secondary/TestUtil';

import { makePOI } from './POI.fixture';
import { stubLogger } from './TestUtil';

import { NotFound } from '@/domain/NotFound';
import { POI } from '@/domain/POI';
import { ComponentState } from '@/primary/ComponentState';
import { InteractiveMapComponent, InteractiveMapVue } from '@/primary/interactive-map';
import { POIGeojsonFeature } from '@/primary/interactive-map/POIGeojsonFeature';

let wrapper: Wrapper<InteractiveMapComponent>;
let component: InteractiveMapComponent;

const stubWebmap = () => ({
  setup: sinon.stub(),
  addControl: sinon.stub(),
  fit: sinon.stub(),
  getExtent: sinon.stub(),
  addPOIs: sinon.stub(),
});

const stubPoiRepository = () => ({
  find: sinon.stub().resolves([makePOI()]),
});

const wrap = (webmapStub = stubWebmap(), poiRepositoryStub = stubPoiRepository(), loggerStub = stubLogger()) => {
  const localVue = createLocalVue();
  localVue.use(VueRouter);
  wrapper = shallowMount<InteractiveMapComponent>(InteractiveMapVue, {
    localVue,
    provide: { webmap: () => webmapStub, poiRepository: () => poiRepositoryStub, logger: () => loggerStub },
  });
  component = wrapper.vm;
};

describe('InteractiveMap', () => {
  it('should be a Vue instance', () => {
    wrap();
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('should setup webmap', () => {
    const webmapStub = stubWebmap();
    wrap(webmapStub);
    expect(webmapStub.setup.calledOnceWith('interactive-map')).toBeTruthy();
    expect(webmapStub.addControl.calledOnce).toBeTruthy();
    expect(webmapStub.fit.calledOnce).toBeTruthy();
    expect(webmapStub.fit.getCall(0).args[0]).toEqual([-149.63069915771484, -17.57080239428328, -149.5444393157959, -17.51908083559126]);
  });

  it('should be in pending state by default', async () => {
    const poiRepositoryStub = stubPoiRepository();
    poiRepositoryStub.find.returns(dummyPromise());
    await wrap(undefined, poiRepositoryStub);
    expect(component.state).toBe(ComponentState.PENDING);
  });

  it('should be in error state if POI retrieval fails', async next => {
    const poiRepositoryStub = stubPoiRepository();
    const promise = Promise.reject(new NotFound('POI'));
    poiRepositoryStub.find.returns(promise);
    const logger = stubLogger();
    await wrap(undefined, poiRepositoryStub, logger);
    await promise.catch(() => {
      const [message, error] = logger.error.getCall(0).args;
      expect(message).toBe('Impossible to find POIs');
      expect(error.message).toContain('POI');
      expect(component.state).toBe(ComponentState.ERROR);
      next();
    });
  });

  it('should retrieve POI', async () => {
    await wrap();
    expect(component.state).toBe(ComponentState.SUCCESS);
    const poiFeature = component.poifeatureCollection.features[0];
    expect(poiFeature).toEqual<POIGeojsonFeature>({
      id: 'test',
      type: 'Feature',
      properties: {
        service: 'BAR',
        accessibilityLevel: 'FULLY_ACCESSIBLE',
        tags: { 'addr:housename': 'Fare Tony', amenity: 'bar', name: 'Bar du Fare Tony', note: 'Bar à bière' },
        style: { icon: 'bar', borderColor: '#23d160' },
      },
      geometry: { type: 'Point', coordinates: [-149.5687696, -17.5403938] },
    });
  });
});
