import sinon, { SinonStub } from 'sinon';

import { Logger } from '@/domain/Logger';

export interface LoggerStub extends Logger {
  error: SinonStub;
}

export const stubLogger = () =>
  ({
    error: sinon.stub(),
  } as LoggerStub);
