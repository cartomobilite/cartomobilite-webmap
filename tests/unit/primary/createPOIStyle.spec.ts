import sinon from 'sinon';

import { Feature } from 'ol';
import { Style } from 'ol/style';

import { createPOIStyle, drawImage } from '@/primary/webmap/createPOIStyle';

describe('createPOIStyle', () => {
  it('should create POI style', () => {
    const poiStyleFunction = createPOIStyle();
    const olFeature = new Feature();
    olFeature.setProperties({ style: { icon: 'bar', borderColor: '#23d160' } });
    const style = poiStyleFunction(olFeature);
    expect(poiStyleFunction(olFeature)).toBeInstanceOf(Style);
  });

  it('should draw image', () => {
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
    document.body.append(canvas);
    const image = new Image();
    const spy = sinon.spy(context!, 'drawImage');
    drawImage(context!, image)();
    expect(spy.calledOnce).toBeTruthy();
  });
});
