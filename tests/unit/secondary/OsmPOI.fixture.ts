export const osmPOIs = () => `<?xml version="1.0" encoding="UTF-8"?>
<osm version="0.6" generator="Overpass API 0.7.56.3 eb200aeb">
<note>The data included in this document is from www.openstreetmap.org. The data is made available under ODbL.</note>
<meta osm_base="2020-04-27T07:39:02Z"/>
  <bounds minlat="-17.5708024" minlon="-149.6306992" maxlat="-17.5190808" maxlon="-149.5444393"/>
  <node id="1728471938" lat="-17.5403938" lon="-149.5687696" version="7" timestamp="2014-08-21T15:47:49Z" changeset="24912383" uid="308" user="MichaelCollinson">
    <tag k="addr:housename" v="Fare Tony"/>
    <tag k="amenity" v="bar"/>
    <tag k="name" v="Bar du Fare Tony"/>
    <tag k="note" v="Bar à bière"/>
  </node>
</osm>`;
