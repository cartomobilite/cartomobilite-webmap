import { Feature } from 'ol';
import { Point } from 'ol/geom';

import { osmPOIs } from './OsmPOI.fixture';
import { stubAxiosInstance, stubAxiosNotFound } from './TestUtil';

import { AccessibilityLevel } from '@/domain/AccessibilityLevel';
import { NotFound } from '@/domain/NotFound';
import { POI } from '@/domain/POI';
import { Profile } from '@/domain/Profile';
import { Service } from '@/domain/Service';
import { OsmPOIRepository, toAccessibilityLevel, toCondition, toProfileAccessibilityLevel } from '@/secondary/OsmPOIRepository';

const extent: [number, number, number, number] = [-149.63069915771484, -17.57080239428328, -149.5444393157959, -17.51908083559126];

describe('OsmPOIRepository', () => {
  it('should raise not found error', next => {
    const axiosInstance = stubAxiosInstance();
    axiosInstance.get.rejects(stubAxiosNotFound('No POI found'));
    const osmPOIRepositoryRepository = new OsmPOIRepository(axiosInstance);
    osmPOIRepositoryRepository.find(Service.BAR, Profile.WHEELCHAIR, extent).catch(error => {
      expect(error).toBeInstanceOf(NotFound);
      expect(error.message).toContain('POI');
      expect(error.stack).toContain('Caused by');
      expect(error.stack).toContain('No POI found');
      next();
    });
  });

  it('should return POIs', async () => {
    const axiosInstance = stubAxiosInstance();
    const pois = osmPOIs();
    axiosInstance.get.resolves({ data: pois });
    const osmPOIRepositoryRepository = new OsmPOIRepository(axiosInstance);
    const [poi] = await osmPOIRepositoryRepository.find(Service.BAR, Profile.WHEELCHAIR, extent);
    expect(poi).toEqual<POI>({
      id: '1728471938',
      service: Service.BAR,
      accessibilityLevel: AccessibilityLevel.UNKNOWN,
      tags: { 'addr:housename': 'Fare Tony', amenity: 'bar', name: 'Bar du Fare Tony', note: 'Bar à bière' },
      coordinates: [-149.5687696, -17.5403938],
    });
  });

  it('should return overpass condition for a given service', () => {
    expect(toCondition(Service.BAR)).toBe('amenity=bar');
    expect(toCondition(Service.BUS_STOP)).toBe('highway=bus_stop');
    expect(toCondition(Service.HOTEL)).toBe('tourism=hotel');
    expect(toCondition(Service.RESTAURANT)).toBe('"amenity"~"restaurant|fast_food"');
    expect(toCondition(Service.SUPERMARKET)).toBe('shop=supermarket');
    expect(toCondition(Service.TOILETS)).toBe('amenity=toilets');
  });

  it('should return accessibility level for a given tag value', () => {
    expect(toAccessibilityLevel(undefined)).toBe(AccessibilityLevel.UNKNOWN);
    expect(toAccessibilityLevel('limited')).toBe(AccessibilityLevel.PARTIALLY_ACCESSIBLE);
    expect(toAccessibilityLevel('yes')).toBe(AccessibilityLevel.FULLY_ACCESSIBLE);
    expect(toAccessibilityLevel('no')).toBe(AccessibilityLevel.NOT_ACCESSIBLE);
    expect(toAccessibilityLevel('default')).toBe(AccessibilityLevel.UNKNOWN);
  });

  it('should return accessibility level for a given feature and profile', () => {
    const olFeature = new Feature<Point>();
    expect(toProfileAccessibilityLevel(olFeature, Profile.WHEELCHAIR)).toBe(AccessibilityLevel.UNKNOWN);
    expect(toProfileAccessibilityLevel(olFeature, Profile.ELDER)).toBe(AccessibilityLevel.UNKNOWN);
    expect(toProfileAccessibilityLevel(olFeature, Profile.DEAF)).toBe(AccessibilityLevel.UNKNOWN);
    expect(toProfileAccessibilityLevel(olFeature, Profile.BLIND)).toBe(AccessibilityLevel.UNKNOWN);
  });
});
