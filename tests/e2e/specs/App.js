/// <reference types="Cypress" />

import { dataSelector } from '../support/dataSelector';

const navbarSelector = name => dataSelector(`navbar.${name}`);
const leftbarSelector = name => dataSelector(`leftbar.${name}`);

describe('App', () => {
  it('should display nav bar', () => {
    cy.visit('/');
    cy.get(navbarSelector('image'));
    cy.contains(navbarSelector('title'), "Cartomobil'ité");
  });

  it('should toggle left bar', () => {
    cy.visit('/');
    cy.get(dataSelector('leftbar'));
    cy.get(leftbarSelector('closer')).should('not.be.visible');
    cy.get(leftbarSelector('activator')).click();
    cy.get(leftbarSelector('activator')).should('not.be.visible');
    cy.get(leftbarSelector('closer')).click();
    cy.get(leftbarSelector('closer')).should('not.be.visible');
    cy.get(leftbarSelector('activator'));
  });
});
